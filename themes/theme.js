export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#1C4D92',
      accent: '#3BA7A2',
      secondary: '#0099CC',
      success: '#2B7B2E',
      info: '#5FE3E2',
      warning: '#FB9400',
      error: '#E81515'
    },
    dark: {
      primary: '#1C4D92',
      accent: '#3BA7A2',
      secondary: '#0099CC',
      success: '#2B7B2E',
      info: '#5FE3E2',
      warning: '#FB9400',
      error: '#E81515'
    }
  }
}
