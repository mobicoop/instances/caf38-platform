-- ticket#21678
UPDATE `paragraph` SET `text` = '<p>Oui, je peux.</p><ul><li>Dans le cadre d’un déplacement avec un véhicule personnel, la décision appartient au conducteur. Rappelons qu’à ce jour, le covoiturage ne fait l’objet d’aucune réglementation particulière qui le distingue de l’usage courant d’un véhicule autorisant le transport d’un conjoint, d’un ami, d’un collègue ou d’un enfant. Néanmoins, la Fédération Française des Assurances conseille qu’une déclaration soit faite, à titre préventif, pour se prémunir contre un refus d’indemnisation de l’assurance en raison de l’article L 113-2 du code des assurances qui fait obligation de déclarer toutes circonstances nouvelles qui auraient pour conséquence d’aggraver les risques ou d’en créer de nouveaux.</li><li>Dans le cadre d’un déplacement avec un véhicule de service, la CAF38 autorise le transport d’un collègue et de son enfant dans le véhicule de service.</li></ul>' WHERE `paragraph`.`id` = 26;

UPDATE `paragraph` SET `text` = "<p>Rien de plus simple ! Vous pouvez utiliser le<a href='https://caf38.mobiride.fr/contact'> formulaire de contact</a> sur le site.</p>" WHERE `paragraph`.`id` = 43;


